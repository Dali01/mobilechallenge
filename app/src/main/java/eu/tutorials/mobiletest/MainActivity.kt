package eu.tutorials.mobiletest

import android.app.Activity
import android.content.Intent

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import eu.tutorials.mobiletest.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(){

    private var binding: ActivityMainBinding? = null
    private var adapter : QuizItemsAdapter? = null
    private var submitClicked : Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        val imageList = ArrayList<Image>()
        imageList.add(Image(R.drawable.boy_left,false,R.drawable.start))
        imageList.add(Image(R.drawable.boy_left2,false,R.drawable.start))
        imageList.add(Image(R.drawable.boy_right,false,R.drawable.start))
        imageList.add(Image(R.drawable.fille_right,false,R.drawable.start))

        binding?.rvQuestion?.layoutManager = LinearLayoutManager(this@MainActivity, RecyclerView.HORIZONTAL,false)
        adapter = QuizItemsAdapter(imageList,submitClicked)
        binding?.rvQuestion?.adapter = adapter

        binding?.submit?.setOnClickListener {
            binding?.submit?.setImageResource(R.drawable.check_hover)
            if (submitClicked) {
                submitClicked = false
                for (image in imageList) {
                    if (image.clicked) {
                        if (image.imageSrc == R.drawable.boy_right || image.imageSrc == R.drawable.boy_left) {
                            image.backgroudCol = R.drawable.correct_1
                            adapter?.notifyDataSetChanged()
                        } else {
                            image.backgroudCol = R.drawable.wrong_1
                            adapter?.notifyDataSetChanged()
                        }
                    }

                }
                adapter = QuizItemsAdapter(imageList,submitClicked)
                binding?.rvQuestion?.adapter = adapter
            }
        }



//        adapter?.setOnClickListener(object : QuizItemsAdapter.OnClickListener{
//            override fun onClick(position: Int , holder: QuizItemsAdapter.QuestionViewHolder) {
//                holder.item.setImageResource(R.drawable.click_stat)
//                adapter?.notifyDataSetChanged()
//            }
//        })
    }

    private fun submitClicked() {

    }



}