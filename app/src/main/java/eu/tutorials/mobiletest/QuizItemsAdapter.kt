package eu.tutorials.mobiletest

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import eu.tutorials.mobiletest.databinding.ItemQuestionBinding

class QuizItemsAdapter(
    private val items : ArrayList<Image>,
    private val submit : Boolean
) : RecyclerView.Adapter<QuizItemsAdapter.QuestionViewHolder>() {

    private var onClickListener: OnClickListener? = null

    inner class QuestionViewHolder(binding : ItemQuestionBinding)
        :RecyclerView.ViewHolder(binding.root){
        val item = binding.item
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuestionViewHolder {
        return QuestionViewHolder(ItemQuestionBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    fun setOnClickListener(onClickListener: OnClickListener){
        this.onClickListener = onClickListener
    }

    override fun onBindViewHolder(holder: QuestionViewHolder, position: Int) {
        holder.item.setImageResource(items[position].imageSrc)
        holder.itemView.setBackgroundResource(items[position].backgroudCol)
        holder.itemView.setOnClickListener {
            if (submit) {
                if (!items[position].clicked) {
                    holder.itemView.setBackgroundResource(R.drawable.click_stat)
                    items[position].clicked = true
                } else {
                    holder.itemView.setBackgroundResource(R.drawable.start)
                    items[position].clicked = false
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    interface OnClickListener{
        fun onClick(position: Int , holder: QuestionViewHolder)
    }
}